import React from 'react'
import './ProductCart.css'
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { addToCart } from '../../redux/boorivaSlice';
import { Button, message, Space } from 'antd';

const ProductsCart = ({product}) => {
  const dispatch = useDispatch()
  // console.log(product);
  const  navigate =useNavigate()

  const _id =product.title

  const idString = (_id) =>String(_id).toLowerCase().split("").join("")

  const rootId = idString(_id)


  const handleDetails =()=>{
   console.log(`details`);
   navigate(`/product/${rootId}`,{
    state:{
        item:product,
    }
   })
   



  }




const [messageApi, contextHolder] = message.useMessage();
const success = () => {
  messageApi.open({
    type: 'success',
    content:`${product.title} довавлен в корзину`,
  });
};

  return (
    <div >
         <div onClick={handleDetails} className='product__block'>
          <img className='product__image group-hover:scale-110 duration-500' src={product.image} alt="product" />
         </div>

         <div className='product__info'>
            <div>
               <h2>{product.title}</h2>
            </div>
               <div className='product__info__price '>
                <div className='product__info__price_wrapper '>
                    <p className='product__info__price__old'>{product.oldPrice}&#8364;</p>
                    <p className='product__info__price__set'>{product.price}&#8364;</p>
                </div>
                
                  
   
    

               </div>
              
         </div>
         {contextHolder}
                <Space>


                <Button  onClick={success}  >
                  <p onClick={()=>dispatch(addToCart({
                    _id:product._id,
                    title:product.title,
                    image:product.image,
                    price:product.price,
                    quantity:1,
                    description:product.description,

                  }))}>Add to cart</p></Button>
                </Space>
    </div>
  )
}

export default ProductsCart

