import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import './Product.css'
import { useDispatch } from 'react-redux'

import { addToCart } from '../../redux/boorivaSlice';
import { Image,Button, message, Space  } from 'antd';

const Product = () => {
const dispatch = useDispatch()

const[details,setDetails]=useState({})
let [baseQant,setQatn]=useState(1)

  const Location = useLocation()
  useEffect(()=>{
     setDetails(Location.state.item)
  },[])


  const [messageApi, contextHolder] = message.useMessage();
const success = () => {
  messageApi.open({
    type: 'success',
    content:`${details.title} довавлен в корзину (${baseQant}шт.)`,
  });
};
  return (
<>  
<div className='product__wrapper'>
    <div>
      <Image
    width={200} className=' product__images'src={details.image}/>
    </div>
<div className='product__details'>
    <div>
      <h2>{details.title}</h2>
    </div>
     <div>
     <p className='product__info__price__set'>{details.price*baseQant}&#8364;</p>
      <p>{details.description}</p>
     </div>
     

      <div className='quantity__block flex items-center gap-4 text-sm font-semibold'>
        <p>Quantity</p>
        <div className='Qant__btns'>
        <button  className='Qant__btn'   onClick={()=>setQatn(baseQant===1?baseQant=1:baseQant-1)}>-</button>
        <span>{baseQant}</span>
        <button className='Qant__btn' onClick={()=>setQatn(baseQant+1)}>+</button>
        </div>
        <div>

        {contextHolder}
                <Space>
          <Button onClick={success}   > 
          <p onClick={()=>dispatch(addToCart({
            _id:details._id,
            title:details.title,
            image:details.image,
            price:details.price,
            quantity:baseQant,
            description:details.description,

          }))}>Add to cart</p>
          
          </Button>
          </Space>
        </div>
       
       
      </div>

      <div>
          <p>Category:{details.category}</p>
        </div>
      
 </div>



      </div>
      <div>
     
      </div>
      <div>

      

      </div>
      
    </>
  )
}

export default Product
