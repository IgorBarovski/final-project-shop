import React from 'react'
import ProductsCart from '../ProductCart/ProductsCart'
import './index.css'



const Products = ({products}) => {

  return (
    <> 
    
    <h1>In this store you will find one-stop solutions

    </h1>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas sequi praesentium nemo aperiam libero magnam, quae voluptatum non, sapiente culpa, laboriosam dignissimos deserunt temporibus impedit fuga eligendi magni deleniti fugit.</p>
    
    <div className='products__cart grid-cols-4 gap-10' >
     {
      products.map((item)=>(
          <ProductsCart key={item._id} product={item}/>
      ))}
     
    </div>
    
    
    
    </>
   

  
  )
}

export default Products
