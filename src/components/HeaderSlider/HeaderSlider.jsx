import React, { useState } from 'react'
import { LeftOutlined,RightOutlined } from '@ant-design/icons';
import './index.css'

const HeaderSlider = () => {

  const [currentSlide,setSliders]=useState(0)
    const data = [
        "https://amazonproone.vercel.app/static/media/img5.aa945e25375bfdee385f.jpg",
        "https://amazonproone.vercel.app/static/media/img2.bc1bdb910ead16c65197.jpg" ,
        "https://amazonproone.vercel.app/static/media/img1.efb3d39101f7ef77d616.jpg" ,
        "https://amazonproone.vercel.app/static/media/img4.8f7fc6b56e74dba2b6f9.jpg" 
    ]

   

    const nextSlide =()=>{
      setSliders(currentSlide===0 ? 1 : (prev) => prev+1  )
 
  }
  

  const prevSlide =()=>{
    setSliders(currentSlide===0 ? 3 : (prev) => prev-1 )
}
  

  return (
   <>
   <div className='block__slider    '>
        <div className='block__slider__wrapper  '>
          
      
              <div   className='items__image ' style={{transform:`translateX(-${currentSlide * 100}vw)`}}>
              <img className='item__image  ' src={data[0]} alt="image__slider1" />
              <img className='item__image ' src={data[1]} alt="image__slider2" />
              <img className='item__image' src={data[2]} alt="image__slider3" />
              <img className='item__image ' src={data[3]} alt="image__slider4" />
            </div>
           
            <div className=' block__buttons__slider '>

                <div  onClick={prevSlide}  className='btn__slider' ><LeftOutlined/></div>
                <div onClick={nextSlide} className='btn__slider'><RightOutlined/></div>

          </div>

                 

          
        </div>

   </div>
   
   
   </>
  )
}

export default HeaderSlider
