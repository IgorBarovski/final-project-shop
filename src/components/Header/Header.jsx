import React from 'react'
import { HeadLogo,Vectorbasket } from '../../arsenal'
import { useParams, Link } from "react-router-dom"
import './Header.css'
import { useSelector } from 'react-redux'
import Shop from '../../pages/Shop'







const Header = () => {
  const productData =useSelector((state)=>state.booriva.productData)
  console.log(productData);

  return (
    <div className=' wrapper__header w-full h-20 border-b-2 border-b-gray-300  sticky top-0 z-50 bg-white'>

      <div className=' header__top flex items-center justify-between  max-w-screen-xl mx-auto p-5'>

      <Link to="/"> <div >

<img className= 'head__logo w-28' src={HeadLogo} alt='HeadLogo'/>
</div></Link>
      
        
           

            <div className='nav flex items-center gap-6'>

                <ul className='nav__items flex items-center gap-6'>
                  <li>Home</li>
                  <li>Pages</li>
                  <Link to='../../pages/Shop'>  
                  <li>Shop</li>
                  </Link>
                
                  <li>Element</li>
                  <li>Blog</li>
                </ul>
              <Link to='./cart'>
              <div className='nav__items__basket relative flex items-center gap-6'>
                <img className='nav__items__basket__image w-8'  src={Vectorbasket} alt='ImageBasket'/>

                <span className='nav__items__basket__count  absolute  top-3 left-3 text-sm flex items-center justify-center font-titleFont font-medium text-sky-200'>{productData.length}</span>
                <div >
                <img className='nav__items__basket__acount  w-8 h-8 rounded-full'  src='https://images.pexels.com/photos/2129796/pexels-photo-2129796.png?auto=compress&cs=tinysrgb&w=600' alt='ImageBasket'/>
                </div>
              </div>
              </Link>
             
            
            
            </div>
          
      </div>
      
    </div>
  )
}

export default Header
