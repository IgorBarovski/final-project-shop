import React from "react";
import Home from "./pages/Home";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Cart from "./pages/Cart"
import Order from "./pages/Order"
import {
  createBrowserRouter,
  Outlet,
  RouterProvider,
  ScrollRestoration,
} from "react-router-dom";
import { productsData } from "./api/Api";
import Product from "./components/Product/Product";
import Shop from "./pages/Shop";

const Layout = ()=>{
  return(
    <div> 
    <Header/>

    <Outlet/>
   <ScrollRestoration/>
    <Footer/>
  </div>
  )
  
}

const router =createBrowserRouter([

  {
    path:"/",
    element:<Layout/>,
    children:[
      {
        path:"/",
        element:<Home/>,
        loader:productsData
      },
      {
        path:"/product/:id",
      element:<Product/>,

      },
      {
        path:"cart",
        element:<Cart/>,
      },
      {
        path:"order",
        element:<Order/>,
      },
      {
        path:"shop",
        element:<Shop/>,
      },
    ],
  },
])



function App() {
  return (
    <div className="App">
        <div className="wrapper">

            <div className="main">
            <RouterProvider router={router}/>
            </div>
      </div>
    </div>
  );
}

export default App;
